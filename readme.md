## 1. 문제

- 코레오그래피 사가에서 ms내의 로컬 트랜잭션과 이벤트 퍼블리싱이 아토믹하지 않은 문제가 있음  
  ex) 로컬 트랜잭션은 성공했는데 카프카가 죽어있으면 이벤트 퍼블리싱은 실패함
- 따라서, saga의 안정성을 위해서는 반드시 로컬 트랜잭션과 이벤트 퍼블리싱이 아토믹한 동작임을 보장해야 함!!

## 2. 해결방법

### CDC(Changed Data Capture)

- 로컬 트랜잭션 이후 이벤트 퍼블리싱하는 동작을 outbox라는 db 테이블에 insert 하는걸로 대체
    - 같은 트랜잭션을 사용함으로써 이 두 동작은 아토믹함이 보장됨
- 이벤트 퍼블리싱은 outbox 테이블을 관찰하고 있는 CDC가 대신해줌
    - CDC는 outbox 테이블의 변화를 감지하고 kafka에 이벤트를 전송함

## 3. 데모 프로젝트 소개

CDC는 debezium-connect, database는 mysql을 사용했음
(outbox, deadTopic 테이블의 컬럼은 debezium docs를 참고함)

### 켜는법

- kafka-cluster 실행 (docker-compose -f kafka-cluster.yml -p kafka-cluster up -d)
- debezium이 정상작동하면 debezium.txt에 있는 curl 실행
    - 관찰해야할 outbox 테이블을 알려주는 작업
    - database는 mysql 사용
- spring boot 어플리케이션 실행

### 테스트 방식

- [POST] /user에 request를 요청 (RestController.kt)
- user, outbox 테이블에 새로운 raw를 insert함
- outbox에 insert가 정상적으로 되면 CDC가 kafka에 이벤트를 퍼블리싱함
- 다른 MS에서는 이벤트를 구독해서 핸들링함 (KafkaEventListener.kt)

### 장애 처리

CDC를 사용하면 이벤트 퍼블리싱까지는 문제 없지만,      
이벤트를 핸들링하는 쪽에서 장애가 발생해서 이벤트 핸들링이 안되면 롤백이 필요한 문제는 여전함

- kafka event commit은 수동으로
    - 완벽하게 이벤트 핸들링이 된 경우에만 커밋
    - 바로 롤백이 아닌 재시도가 가능함
- dead-topic을 별도로 관리
    - 이벤트를 핸들링하는 쪽에서 재시도를 해도 해결이 안되면 해당 이벤트를 dead-event 처리
        - 이벤트를 발급한 쪽에서는 dead-topic을 구독하고 있음
        - dead-topic이 오면 필요한 rollback을 진행
        - 핸들링한 dead-event와 더불어 본인이 퍼블리싱한 event까지 수동으로 직접 커밋함 