package com.example.debezinumDemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DebezinumDemoApplication

fun main(args: Array<String>) {
    runApplication<DebezinumDemoApplication>(*args)
}
