package com.example.debezinumDemo.domain.repository

import com.example.debezinumDemo.domain.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<User, Int>
