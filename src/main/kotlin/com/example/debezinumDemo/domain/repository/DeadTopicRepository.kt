package com.example.debezinumDemo.domain.repository

import com.example.debezinumDemo.domain.entity.DeadTopic
import org.springframework.data.jpa.repository.JpaRepository

interface DeadTopicRepository : JpaRepository<DeadTopic, Int>
