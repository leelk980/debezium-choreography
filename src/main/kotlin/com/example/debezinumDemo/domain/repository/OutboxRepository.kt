package com.example.debezinumDemo.domain.repository

import com.example.debezinumDemo.domain.entity.Outbox
import org.springframework.data.jpa.repository.JpaRepository

interface OutboxRepository : JpaRepository<Outbox, Int>
