package com.example.debezinumDemo.domain.entity

import javax.persistence.*

@Entity
@Table(name = "user")
open class User(
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    open var id: Int,

    @Column(name = "name", nullable = false)
    open var name: String
)
