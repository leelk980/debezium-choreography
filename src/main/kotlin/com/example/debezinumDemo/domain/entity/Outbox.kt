package com.example.debezinumDemo.domain.entity

import javax.persistence.*

@Entity
@Table(name = "_outbox")
open class Outbox(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int,

    @Column(name = "aggregateid", nullable = false)
    open var aggregateid: Int,

    @Column(name = "aggregatetype", nullable = false)
    open var aggregatetype: String,

    @Column(name = "type", nullable = false)
    open var type: String,

    @Column(name = "payload", nullable = false)
    open var payload: String
)
