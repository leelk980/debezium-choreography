package com.example.debezinumDemo.domain.entity

import javax.persistence.*

@Entity
@Table(name = "_dead_topic")
open class DeadTopic(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Int,

    @Column(name = "aggregatetype", nullable = false)
    open var aggregatetype: String,

    @Column(name = "aggregateid", nullable = false)
    open var aggregateid: Int,

    @Column(name = "type", nullable = false)
    open var type: String,

    @Column(name = "payload", nullable = false)
    open var payload: String
)
