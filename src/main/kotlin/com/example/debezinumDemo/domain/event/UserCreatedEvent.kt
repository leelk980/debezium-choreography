package com.example.debezinumDemo.domain.event

data class UserCreatedEvent(
    val type: String = Companion.type,
    val payload: Payload
) {
    companion object {
        const val type = "user.created"
    }

    data class Payload(
        val id: Int,
        val name: String
    )
}
