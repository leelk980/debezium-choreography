package com.example.debezinumDemo.config

import com.example.debezinumDemo.domain.entity.DeadTopic
import com.example.debezinumDemo.domain.repository.DeadTopicRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.listener.CommonErrorHandler
import org.springframework.kafka.listener.ContainerProperties
import org.springframework.kafka.listener.DefaultErrorHandler
import org.springframework.util.backoff.FixedBackOff

@Configuration
@EnableKafka
class KafkaConfig(
    @Value("\${kafka.bootstrapAddress}")
    private val bootstrapAddress: String,
    private val objectMapper: ObjectMapper,
    private val deadTopicRepository: DeadTopicRepository
) {
    companion object {
        const val groupId = "ms_name"
        const val eventPrefix = "outbox.event."
        const val deadEventPrefix = "outbox.event.dead."
        const val containerFactory = "consumerContainerFactory"
    }

    @Bean
    fun consumerFactory(): ConsumerFactory<String, String> {
        val config = mutableMapOf<String, Any>()
        config[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapAddress
        config[ConsumerConfig.GROUP_ID_CONFIG] = groupId
        config[ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG] = false
        config[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
        config[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java

        return DefaultKafkaConsumerFactory(config)
    }

    @Bean
    fun consumerContainerFactory(): ConcurrentKafkaListenerContainerFactory<String, String> {
        val factory = ConcurrentKafkaListenerContainerFactory<String, String>()
        factory.consumerFactory = consumerFactory()
        factory.containerProperties.ackMode = ContainerProperties.AckMode.MANUAL_IMMEDIATE
        factory.setCommonErrorHandler(commonErrorHandler())

        return factory
    }

    private fun commonErrorHandler(): CommonErrorHandler {
        val errorHandler = DefaultErrorHandler(
            { event, exception ->
                try {
                    println("[KafkaErrorHandler] event=[$event], errorMessage=[${exception.message}]")

                    val shouldBeDeadTopic = true
                    if (shouldBeDeadTopic) {
                        val topic = event.topic().split(eventPrefix).last()
                        val payload = event.value() as String
                        val aggregateType = event.topic().split(".")[2]
                        val aggregateId = objectMapper.readTree(payload).get("id")?.asInt() ?: 0

                        deadTopicRepository.save(
                            DeadTopic(
                                id = 0,
                                aggregatetype = aggregateType,
                                aggregateid = aggregateId,
                                payload = "${event.partition()}:${event.offset()}:$payload",
                                type = "dead.$topic"
                            )
                        )
                    }
                } catch (err: Throwable) {
                    println(err)
                }
            },

            FixedBackOff(1000, 1)
        )

        return errorHandler
    }
}
