package com.example.debezinumDemo

import com.example.debezinumDemo.domain.entity.Outbox
import com.example.debezinumDemo.domain.entity.User
import com.example.debezinumDemo.domain.event.UserCreatedEvent
import com.example.debezinumDemo.domain.repository.OutboxRepository
import com.example.debezinumDemo.domain.repository.UserRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import kotlin.math.roundToInt

@org.springframework.web.bind.annotation.RestController
class RestController(
    private val userRepository: UserRepository,
    private val outboxRepository: OutboxRepository,
    private val objectMapper: ObjectMapper
) {
    @PostMapping("/user")
    @Transactional
    fun createUser(): Int {
        // auth.finduserbyname() => validation .. internalcall

        // domain logic
        val randomNumber = Math.random().times(1000).roundToInt()
        val newUser = userRepository.save(
            User(
                id = 0,
                name = "random$randomNumber"
            )
        )

        // insert event
        outboxRepository.save(
            Outbox(
                id = 0,
                aggregateid = newUser.id,
                aggregatetype = "user",
                type = UserCreatedEvent.type,
                payload = objectMapper.writeValueAsString(
                    UserCreatedEvent.Payload(newUser.id, newUser.name)
                )
            )
        )

        return newUser.id
    }
}
