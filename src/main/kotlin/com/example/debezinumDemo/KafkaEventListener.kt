package com.example.debezinumDemo

import com.example.debezinumDemo.config.KafkaConfig
import com.example.debezinumDemo.domain.event.UserCreatedEvent
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.OffsetAndMetadata
import org.apache.kafka.common.TopicPartition
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.Acknowledgment
import org.springframework.stereotype.Component

@Component
class KafkaEventListener(
    private val objectMapper: ObjectMapper
) {
    @KafkaListener(
        topics = [KafkaConfig.eventPrefix + UserCreatedEvent.type],
        groupId = KafkaConfig.groupId,
        containerFactory = KafkaConfig.containerFactory
    )
    fun handleUserCreateEvent(event: ConsumerRecord<String, String>, ack: Acknowledgment) {
        println("event = $event")

        // logic with payload
        val payload = objectMapper.readValue(event.value(), UserCreatedEvent.Payload::class.java)

        throw Exception("")

        if (false) {
            ack.acknowledge()
        }
    }

    @KafkaListener(
        topics = [KafkaConfig.deadEventPrefix + UserCreatedEvent.type],
        groupId = KafkaConfig.groupId,
        containerFactory = KafkaConfig.containerFactory
    )
    fun handleDeadTopicEvent(
        event: ConsumerRecord<String, String>,
        ack: Acknowledgment,
        consumer: Consumer<String, String>
    ) {
        println("event = $event")

        val topic = event.topic().replace("dead.", "")
        val (partition, offset, payload) = event.value().split(":")

        // logic with payload

        // commit origin event
        consumer.commitSync(
            mapOf(
                TopicPartition(topic, partition.toInt())
                    to OffsetAndMetadata(offset.toLong() + 1, null)
            )
        )

        // commit dead-topic event
        ack.acknowledge()
    }
}

// 1. event-broker
// rollback 구현이 필수적임
// orchestration saga에서 main ms의 로직이 커짐
// kafka가 ms간의 dependency를 끊어줌

// 2. eventuate => debezium
// eventuate에 코드를 맞춰야함
// debezium을 쓰면 더 유연
// https://eventuate.io/docs/javav2/java-aggregates.html

// 3. error-handling
// 장애처리는 기본적으로 retry
// 필요시에만 dead-topic 활용해서 rollback

// 4. config
// manual commit
// retry 1000, 1
